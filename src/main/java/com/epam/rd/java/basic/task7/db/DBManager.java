package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance = new DBManager();

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private static String DB_URL;

	private DBManager() {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("app.properties"));
			DB_URL = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Connection getConnection() throws SQLException{
		return DriverManager.getConnection(DB_URL);
	}

	public List<User> findAllUsers() throws DBException {
		try(Connection con = getConnection()) {
			//System.out.println("Got connection");
			Statement stmt = con.createStatement();
			//System.out.println("created stmt");
			List<User> userList = new LinkedList<>();
			ResultSet rs = stmt.executeQuery("SELECT * FROM users");
			//System.out.println("executed query");
			while (rs.next()){
				User user = new User(rs.getInt("id"), rs.getString("login"));
				userList.add(user);
			}
			return userList;
		} catch (SQLException e) {
			System.err.println("findAllUsers got sql exception");
			return null;
		}
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement("INSERT INTO users VALUES (DEFAULT , ?)");
			stmt.setString(1, user.getLogin());
			int rows = stmt.executeUpdate();
			return rows == 1;
		} catch (SQLException sqlException){
			System.err.println("insertUser got sql exception");
			return false;
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		return false;
	}

	public User getUser(String login) throws DBException {
		try(Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM users WHERE users.login = ?");
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				return new User(rs.getInt("id"), rs.getString("login"));
			}
			return null;
		} catch (SQLException sqlException){
			System.err.println("getUserByLogin got sql exception");
			return null;
		}
	}

	public Team getTeam(String name) throws DBException {
		try(Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM teams WHERE teams.name = ?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				return new Team(rs.getInt("id"), rs.getString("name"));
			}
			return null;
		} catch (SQLException sqlException){
			System.err.println("getUserByLogin got sql exception");
			return null;
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try(Connection con = getConnection()) {
			//System.out.println("Got connection");
			Statement stmt = con.createStatement();
			//System.out.println("created stmt");
			List<Team> teamList = new LinkedList<>();
			ResultSet rs = stmt.executeQuery("SELECT * FROM teams");
			//System.out.println("executed query");
			while (rs.next()){
				Team team = new Team(rs.getInt("id"), rs.getString("name"));
				teamList.add(team);
			}
			return teamList;
		} catch (SQLException e) {
			System.err.println("findAllTeams got sql exception");
			return null;
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement("INSERT INTO teams VALUES (DEFAULT , ?)");
			stmt.setString(1, team.getName());
			int rows = stmt.executeUpdate();
			return rows == 1;
		} catch (SQLException sqlException){
			System.err.println("insertTeam got sql exception");
			return false;
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try{
			con = getConnection();
			//System.out.println("got connection");
			con.setAutoCommit(false);
			//System.out.println("setAutoCommit to false");
			PreparedStatement stmt = con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
			int userId = getUser(user.getLogin()).getId();
			for(Team team : teams){
				stmt.setInt(1, userId);
				//System.out.println("Set user id");
				int teamId = getTeam(team.getName()).getId();
				stmt.setInt(2, teamId);
				//System.out.println("Set team id");
				//System.out.println(userId + " " + teamId);
				stmt.executeUpdate();
				//System.out.println("Executed update");
			}
			con.commit();
			//System.out.println("commited");
			return true;
		} catch (SQLException sqlException){
			if(con != null){
				try {
					con.rollback();
					throw new DBException("Transaction error", sqlException);
					//System.out.println("Rollback successful");
				} catch (SQLException sqlException1){
					System.err.println("Exception at rollback");
					throw new DBException("rollback error", sqlException1);
				}
			}
			System.err.println("setTeamsForUser got sql exception");
			return false;
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(Connection con = getConnection()) {
//			{
//				Connection cnt = getConnection();
//				ResultSet rs = cnt.createStatement().executeQuery("SELECT * FROM users_teams");
//				while (rs.next()){
//					System.out.println(rs.getInt("user_id") + " " + rs.getInt("team_id"));
//				}
//			}
			PreparedStatement stmt = con.prepareStatement("SELECT t.id AS id, t.name AS name FROM users_teams INNER JOIN teams t ON users_teams.team_id = t.id WHERE user_id = ?");
			int userId = getUser(user.getLogin()).getId();
			System.out.println(userId);
			stmt.setInt(1, userId);
			ResultSet rs = stmt.executeQuery();
			List<Team> teams = new LinkedList<>();
			while (rs.next()){
				teams.add(new Team(rs.getInt("id"), rs.getString("name")));
			}
			return teams;
		} catch (SQLException sqlException){
			System.err.println("getUserTeams git sql exception");
			return null;
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(Connection con = getConnection()) {
			 int teamId = getTeam(team.getName()).getId();
			 PreparedStatement stmt = con.prepareStatement("DELETE FROM teams WHERE id = ?");
			 stmt.setInt(1, teamId);
			 stmt.executeUpdate();
			 return true;
		} catch (SQLException sqlException){
			System.err.println("deleteTeam got sql exception");
			return false;
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement("UPDATE teams SET name=? WHERE id=?");
			stmt.setString(1, team.getName());
			stmt.setInt(2, getTeam(team.getOldName()).getId());
			stmt.executeUpdate();
			System.out.println("Updated team");
			return true;
		} catch (SQLException sqlException){
			System.err.println("updateTeam got sql exception");
			return false;
		}
	}

}
