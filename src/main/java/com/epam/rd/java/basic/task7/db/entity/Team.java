package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	private String oldName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		oldName = this.name;
		this.name = name;
	}

	public String getOldName(){
		return oldName;
	}

	private Team(String name){
		this.name = name;
	}

	public Team(int id, String name){
		this.id = id;
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Team team = (Team) o;

		return name.equals(team.name);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public String toString() {
		return this.name;
	}
}
